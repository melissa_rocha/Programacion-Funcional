package mx.uach.fing.tienda;

public class Producto {

    private Integer claveProducto;
    private String descripcion;
    private Double precio;
    private Clasificacion clasificacion;
    private Integer existencia;
    private Integer minima;
    private Integer maxima;

    public Producto(Integer claveProducto, String descripcion, Double precio, Clasificacion clasificacion, Integer existencia,
                    Integer minima, Integer maxima){

        this.claveProducto = claveProducto;
        this.descripcion = descripcion;
        this.precio = precio;
        this.clasificacion = clasificacion;
        this.existencia = existencia;
        this.minima = minima;
        this.maxima = maxima;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getClaveProducto() {
        return claveProducto;
    }

    public void setClaveProducto(Integer claveProducto) {
        this.claveProducto = claveProducto;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public Clasificacion getClasificacion() {
        return clasificacion;
    }

    public void setClasificacion(Clasificacion clasificacion) {
        this.clasificacion = clasificacion;
    }

    public Integer getExistencia() {
        return existencia;
    }

    public void setExistencia(Integer existencia) {
        this.existencia = existencia;
    }

    public Integer getMinima() {
        return minima;
    }

    public void setMinima(Integer minima) {
        this.minima = minima;
    }

    public Integer getMaxima() {
        return maxima;
    }

    public void setMaxima(Integer maxima) {
        this.maxima = maxima;
    }



}
