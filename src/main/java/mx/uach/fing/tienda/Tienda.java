package mx.uach.fing.tienda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class Tienda {

    public static void main(String[] args){

        List<Producto> inventario = new ArrayList<Producto>();
        Integer clave = 1;
        Random rn = new Random();

        for (String producto: Arrays.asList ("GALLETAS MARIA GAMESA" , "HARINA DE MAÍZ MINSA" , "HARINA DE TRIGO 3 ESTRELLAS", "PAN DULCE" , "PAN DE CAJA (680 grs.) BIMBO", "TORTILLAS DE MAÍZ  (DEL SUPER)")) {
            inventario.add(new Producto(clave++, producto, new Double(rn.nextInt(100)), Clasificacion.CEREALES, rn.nextInt(100),  0, 100 ));
        }

        for (String producto: Arrays.asList("ARROZ MORELOS", "FRIJOL NEGRO VERACRUZ", "LENTEJA VERDE VALLE", "FRIJOL BAYO")) {
            inventario.add(new Producto(clave++, producto, new Double(rn.nextInt(100)), Clasificacion.GRANOS_SEMILLAS, rn.nextInt(100), 0, 100 ));
        }

        for (String producto: Arrays.asList("GUAYABA", "LIMÓN CON SEMILLA", "MANZANA STARKING", "NARANJA", "PAPAYA", "PLATANO CHIAPAS", "TORONJA", "AGUACATE HASS", "AJO PAQUETE", "CALABACITA", "CEBOLLA",
                "COL", "CHAYOTE", "CHICHARO", "CHILE POBLANO", "CHILE SERRANO", "EJOTE", "ESPINACA", "LECHUGA (OREJONA)", "NOPAL", "PAPA ALPHA", "PEPINO", "JITOMATE BOLA", "TOMATE VERDE", "ZANAHORIA")) {
            inventario.add(new Producto(clave++, producto, new Double(rn.nextInt(100)), Clasificacion.HORTOFRUTÍCOLAS, rn.nextInt(100), 0, 100));
        }

        for (String producto: Arrays.asList("AVES (POLLO ENTERO)", "CARNE DE CERDO FILETE", "CARNE DE RES MILANESA", "CARNES PROCESADAS SALCHICHAS FUD", "JAMÓN FUD VIRGINIA", "CHORIZO FUD CANTIMPALO",
                "PESCADOS Y MARISCOS FRESCOS (MOJARRA TILAPIA)")) {
            inventario.add(new Producto(clave++, producto, new Double(rn.nextInt(100)), Clasificacion.CÁRNICOS, rn.nextInt(100), 0, 100));
        }

        for (String producto: Arrays.asList("ACEITE 1-2-3", "ALIMENTO PARA BEBE. GERBER SOPA CON VEGETALES Y CARNE DE RES (113 GRS)", "ATÚN DOLORES (170 GRS)", "AZÚCAR", "CAFÉ SOLUBLE NESCAFE (100 GRS)",
                "CAFÉ DE GRANO INTERNACIONAL (908 GRS)", "CANELA (50 GRS.)", "CONCENTRADO DE POLLO. KNOR SUIZA (450 GRS)", "CHILES JALAPEÑOS EN LATA LA COSTEÑA (220 GRS)", "CHOCOLATE EN BARRA IBARRA (360 GRS)",
                "CHOCOLATE EN POLVO CHOCO CHOCO (800 GRS)", "DULCES Y POSTRES (GELATINA) D’GARI (140 GRS)", "HUEVO BACHOCO ROJO (18 PIEZAS)", "LECHE CONDENSADA Nestlé 397 GRS)", "LECHE EN POLVO NIDO (360 GRS).",
                "LECHE EVAPORADA CARNATION CLAVEL (470 GRS)", "LECHE PASTEURIZADA LALA", "MANTEQUILLA CHIPILO (90 GRS)", "MAYONESA Mc. CORMICK (390 GRS)", "MOSTAZA Mc. CORMICK (210 GRS)", "PASTA PARA SOPAS LA MODERNA (200 GRS)",
                "PIMIENTA (0.75 GRS)", "PURE DE TOMATE LA COSTEÑA 800 GRS", "QUESO FRESCO PANELA (VOLCANES)", "SAL REFINADA LA FINA", "SARDINA LA TORRE (425 GRS)")) {
            inventario.add(new Producto(clave++, producto, new Double(rn.nextInt(100)), Clasificacion.ABARROTES, rn.nextInt(100), 0, 100 ));
        }

        for (String producto: Arrays.asList("BLANQUEADOR CLORALEX (950 ML.)", "CERILLOS  (10 CAJAS)", "DESODORANTE OBAO(65 ML) ACTIVE FORMEN", "DETERGENTE LAVATRASTES SALVO", "DETERGENTE ROMA (ROPA)", "ESCOBAS", "FOCOS 60 WATTS",
                "INSECTICIDA H-24 (309 ML)DOMESTICO", "JABÓN DE BARRA ZOTE (400 GRS)", "JABÓN DE TOCADOR PALMOLIVE (200 GRS)", "NAVAJAS Y RASTRILLOS PARA AFEITAR. (PRESTO BARBA)", "PAÑALES DESECHABLES. KLEEN BEBE SUAVELASTIC (14 PIEZAS)",
                "PAPEL SANITARIO PETALO (4 ROLLOS)", "PASTA DENTAL COLGATE (100 ML)", "PILAS DURACELL (4 PIEZAS) AA", "SERVILLETAS  PETALO (250 HOJAS)", "SHAMPOO VANART (900 ML).", "TOALLAS SANITARIAS KOTEX FREE SOFT 14 PIEZAS)")) {
            inventario.add(new Producto(clave++, producto, new Double(rn.nextInt(100)), Clasificacion.ARTÍCULOS_LIMPIEZA, rn.nextInt(100), 0, 100 ));
        }

        System.out.println("inventario = " + inventario.size());
        System.out.println("\n");

        System.out.println("Número de productos con existencia mayor a 20.");
        System.out.println(inventario.stream().filter(objeto -> objeto.getExistencia() >20).mapToInt( i -> {return 1;}).sum());
        System.out.println("\n");

        System.out.println("Número de productos con existencia menos a 15.");
        System.out.println(inventario.stream().filter(objeto -> objeto.getExistencia() <15).mapToInt( i -> {return 1;}).sum());
        System.out.println("\n");

        System.out.println("Lista de productos con la misma clasificación y precio mayor 15.50");

        for ( Clasificacion clasificacion: Arrays.asList(Clasificacion.CEREALES, Clasificacion.ABARROTES, Clasificacion.CÁRNICOS, Clasificacion.ARTÍCULOS_LIMPIEZA, Clasificacion.GRANOS_SEMILLAS, Clasificacion.HORTOFRUTÍCOLAS)) {

            inventario.stream().filter(objeto -> objeto.getClasificacion().equals(clasificacion) && objeto.getPrecio() >  15.50).forEach(producto -> {
                System.out.println(producto.getDescripcion() + " -> " + producto.getPrecio());
            });

        }

        System.out.println("\n");
        System.out.println("Lista de productos con precio mayor a 20.30 y menor a 45.00");
        inventario.stream().filter(objeto -> objeto.getPrecio() > 20.0 && objeto.getPrecio() < 45.00).forEach(producto -> {
                    System.out.println(producto.getDescripcion() + " -> " + producto.getPrecio());
        });

        System.out.println("\nNúmero de productos agrupados por su clasificación");

        for ( Clasificacion clasificacion: Arrays.asList(Clasificacion.CEREALES, Clasificacion.ABARROTES, Clasificacion.CÁRNICOS, Clasificacion.ARTÍCULOS_LIMPIEZA, Clasificacion.GRANOS_SEMILLAS, Clasificacion.HORTOFRUTÍCOLAS))

                System.out.println(inventario.stream().filter(objeto -> objeto.getClasificacion().equals(clasificacion)).mapToInt(i -> {return 1;}).sum());

        }

        //System.out.println(inventario.stream().filter(objeto -> objeto.getPrecio() > 15.50).mapToInt( i -> {return 1;}).sum());
    };

