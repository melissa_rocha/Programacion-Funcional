package mx.uach.fing.tienda;

public enum Clasificacion {

    CEREALES,
    GRANOS_SEMILLAS,
    HORTOFRUTÍCOLAS,
    CÁRNICOS,
    ABARROTES,
    ARTÍCULOS_LIMPIEZA,

}
